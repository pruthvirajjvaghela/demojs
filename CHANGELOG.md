# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Changed

### Removed

### Added

## [v1.3.0] - 2024-04-16

### Changed

- All dependencies have been updated to latest versions.

### Removed

- Breaking API change: the `getUtxoAddressList` method has been removed from `ZenottaInstance`.

## [v1.2.15] - 2024-03-10

### Added

- Automatic release publishing to GitLab registry.

### Changed

- Updated build scripts to run CI pipelines on GitLab.
- The package has been renamed from `@zenotta/zenotta-js` to `@zenotta/zenottajs`.
- Switch to use `package-lock.json` only in place of `yarn.lock`.
- Fix linter warnings.

## [v1.2.14] - 2022-12-06

### Added

- Add ability to select address version during key-pair generation.

## [v1.2.12] - 2022-11-09

### Added

- Breaking API change: adding metadata to Receipt creation.

### Changed

- Fix balance limit for 10_000_000_000.
- Update interface for notary signatures.

## [v1.2.11] - 2022-09-22

### Changed

- Return response from failed Axios requests.
- Bug fix broken type guards.
