import json
import sys
import datetime

def categorize_level(level):
    # Categorize levels as "medium", "high", and "low"
    if level.lower() in ["medium", "med"]:
        return "Medium"
    elif level.lower() in ["high"]:
        return "High"
    elif level.lower() in ["low"]:
        return "Low"
    else:
        return "Other"

def generate_html_report(json_file, html_output, project_name, execution_time):
    try:
        with open(json_file, 'r') as f:
            data = json.load(f)
    except FileNotFoundError:
        print(f"Error: JSON file '{json_file}' not found.")
        sys.exit(1)
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON file '{json_file}': {e}")
        sys.exit(1)
    
    current_datetime = datetime.datetime.now().strftime('%d/%m/%Y %H:%M')  # Format adjusted to dd/mm/yyyy hh:mm

    html_content = f"<html><head><title>Semgrep Report for {project_name}</title></head><body>"
    html_content += f"<h1>Security Testing Report for {project_name}</h1>"
    html_content += f"<p><strong>Report Generated Date and Time:</strong> {current_datetime}</p>"
    html_content += f"<p><strong>Total Execution Time:</strong> {execution_time}</p>"
    html_content += "<hr>"

    if 'errors' in data:
        html_content += "<h2>Errors</h2>"
        for error in data['errors']:
            code = error.get('code', 'N/A')
            level = error.get('level', 'N/A')
            message = error.get('message', 'N/A')
            path = error.get('path', 'N/A')
            spans = error.get('spans', [])
            
            html_content += "<div style='border: 1px solid black; padding: 10px; margin-bottom: 10px;'>"
            html_content += f"<p><strong>Code:</strong> {code}</p>"
            html_content += f"<p><strong>Level:</strong> {level}</p>"
            html_content += f"<p><strong>Message:</strong> {message}</p>"
            html_content += f"<p><strong>Path:</strong> {path}</p>"
            
            if spans:
                html_content += "<p><strong>Spans:</strong></p>"
                html_content += "<ul>"
                for span in spans:
                    start = span.get('start', {})
                    end = span.get('end', {})
                    html_content += "<li>"
                    html_content += f"Start - Line: {start.get('line', 'N/A')}, Col: {start.get('col', 'N/A')}, Offset: {start.get('offset', 'N/A')}<br>"
                    html_content += f"End - Line: {end.get('line', 'N/A')}, Col: {end.get('col', 'N/A')}, Offset: {end.get('offset', 'N/A')}"
                    html_content += "</li>"
                html_content += "</ul>"
            
            html_content += "</div>"
    else:
        html_content += "<p>No errors found.</p>"

    if 'interfile_languages_used' in data:
        html_content += "<h2>Interfile Languages Used</h2>"
        html_content += "<ul>"
        for lang in data['interfile_languages_used']:
            html_content += f"<li>{lang}</li>"
        html_content += "</ul>"

    if 'paths' in data:
        html_content += "<h2>Paths Scanned</h2>"
        html_content += "<ul>"
        for path in data['paths']:
            html_content += f"<li>{path}</li>"
        html_content += "</ul>"

    # Aggregating counts for confidence and impact
    confidence_counts = {"High": 0, "Medium": 0, "Low": 0}
    impact_counts = {"High": 0, "Medium": 0, "Low": 0}
    if 'results' in data:
        html_content += "<h2>Results</h2>"
        for result in data['results']:
            check_id = result.get('check_id', 'N/A')
            path = result.get('path', 'N/A')
            start = result.get('start', {})
            end = result.get('end', {})
            extra = result.get('extra', {})
            message = extra.get('message', 'N/A')
            severity = extra.get('severity', 'N/A')
            fingerprint = extra.get('fingerprint', 'N/A')

            confidence = categorize_level(extra.get('metadata', {}).get('confidence', 'N/A'))
            impact = categorize_level(extra.get('metadata', {}).get('impact', 'N/A'))

            # Counting confidence
            if confidence in confidence_counts:
                confidence_counts[confidence] += 1
            else:
                confidence_counts[confidence] = 1

            # Counting impact
            if impact in impact_counts:
                impact_counts[impact] += 1
            else:
                impact_counts[impact] = 1

            html_content += "<div style='border: 1px solid black; padding: 10px; margin-bottom: 10px;'>"
            html_content += f"<p><strong>Check ID:</strong> {check_id}</p>"
            html_content += f"<p><strong>Path:</strong> {path}</p>"
            html_content += f"<p><strong>Message:</strong> {message}</p>"
            html_content += f"<p><strong>Severity:</strong> {severity}</p>"
            html_content += f"<p><strong>Fingerprint:</strong> {fingerprint}</p>"
            
            html_content += "<p><strong>Location:</strong></p>"
            html_content += f"Start - Line: {start.get('line', 'N/A')}, Col: {start.get('col', 'N/A')}, Offset: {start.get('offset', 'N/A')}<br>"
            html_content += f"End - Line: {end.get('line', 'N/A')}, Col: {end.get('col', 'N/A')}, Offset: {end.get('offset', 'N/A')}"
            
            if 'metadata' in extra:
                metadata = extra['metadata']
                html_content += "<p><strong>Metadata:</strong></p>"
                html_content += "<ul>"
                for key, value in metadata.items():
                    if isinstance(value, list):
                        html_content += f"<li><strong>{key.capitalize()}:</strong> {', '.join(str(v) for v in value)}</li>"
                    else:
                        html_content += f"<li><strong>{key.capitalize()}:</strong> {value}</li>"
                html_content += "</ul>"

            html_content += "</div>"
    
    # Creating the table for confidence and impact counts
    html_content += "<h2>Confidence and Impact Counts</h2>"
    html_content += "<table border='1' style='width: 50%;'>"
    html_content += "<tr><th>Type</th><th>Level</th><th>Count</th></tr>"

    for level, count in confidence_counts.items():
        html_content += f"<tr><td>Confidence</td><td>{level}</td><td>{count}</td></tr>"

    for level, count in impact_counts.items():
        html_content += f"<tr><td>Impact</td><td>{level}</td><td>{count}</td></tr>"

    html_content += "</table>"

    html_content += "</body></html>"
    
    with open(html_output, 'w') as f:
        f.write(html_content)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: python generate_html_report.py <input_json_file> <output_html_file> <project_name> <execution_time>")
        sys.exit(1)
    
    input_json_file = sys.argv[1]
    output_html_file = sys.argv[2]
    project_name = sys.argv[3]
    execution_time = sys.argv[4]
    
    generate_html_report(input_json_file, output_html_file, project_name, execution_time)
